import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class GossipShould {

    @Test
    public void be_shared_when_drivers_are_in_the_same_stop() {
        String gossipA = "A";
        String gossipB = "B";
        String gossipC = "C";

        Driver driverA = new Driver(gossipA, 3, 1, 2, 3);
        Driver driverB = new Driver(gossipB, 3, 2, 3, 1);
        Driver driverC = new Driver(gossipC, 4, 2, 3, 4, 5);

        Gossip.getStopsNeededToShare(driverA, driverB, driverC);

        assertThat(driverA.getGossipSet().contains(gossipA)).isTrue();
        assertThat(driverA.getGossipSet().contains(gossipB)).isTrue();
        assertThat(driverA.getGossipSet().contains(gossipC)).isTrue();

        assertThat(driverB.getGossipSet().contains(gossipA)).isTrue();
        assertThat(driverB.getGossipSet().contains(gossipB)).isTrue();
        assertThat(driverB.getGossipSet().contains(gossipC)).isTrue();

        assertThat(driverC.getGossipSet().contains(gossipA)).isTrue();
        assertThat(driverC.getGossipSet().contains(gossipB)).isTrue();
        assertThat(driverC.getGossipSet().contains(gossipC)).isTrue();
    }

    @Test
    public void calculate_the_amount_of_stops_needed_to_share_all_the_gossips() {
        String gossipA = "A";
        String gossipB = "B";
        String gossipC = "C";

        Driver driverA = new Driver(gossipA, 3, 1, 2, 3);
        Driver driverB = new Driver(gossipB, 3, 2, 3, 1);
        Driver driverC = new Driver(gossipC, 4, 2, 3, 4, 5);

        int stops = Gossip.getStopsNeededToShare(driverA, driverB, driverC);

        assertThat(stops).isEqualTo(5);
    }

    @Test
    public void not_calculate_the_amount_of_stops_when_not_possible_to_share_all_the_gossips() {
        String gossipA = "A";
        String gossipB = "B";

        Driver driverA = new Driver(gossipA, 2, 1, 2);
        Driver driverB = new Driver(gossipB, 5, 2, 8);

        int stops = Gossip.getStopsNeededToShare(driverA, driverB);

        assertThat(stops).isEqualTo(0);
    }
}
