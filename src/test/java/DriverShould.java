import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class DriverShould {

    @Test
    public void have_a_starting_gossip() {
        Driver driver = new Driver("A");

        assertThat(driver.getGossipSet().contains("A")).isTrue();
    }

    @Test
    public void have_a_route() {
        Driver driver = new Driver("A", 3, 2, 1);

        assertThat(driver.getRoute()).isNotNull();
    }
}
