import java.util.HashSet;
import java.util.Set;

public class Driver {

    private Set<String> gossipList;
    private int[] route;

    public Driver(String gossip, int...route) {
        gossipList = new HashSet<>();
        gossipList.add(gossip);

        this.route = route;
    }

    public Set<String> getGossipSet() {
        return gossipList;
    }

    public int[] getRoute() {
        return route;
    }
}
