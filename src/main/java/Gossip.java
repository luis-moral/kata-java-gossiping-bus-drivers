import java.util.HashSet;
import java.util.Set;

public class Gossip {

    private static final int TOTAL_STOPS = 480;

    public static int getStopsNeededToShare(Driver...drivers) {
        Set<String> allGossipSet = getAllGossipSet(drivers);

        for (int stopIndex = 0; stopIndex < TOTAL_STOPS; stopIndex++) {
            for (Driver currentDriver : drivers) {
                for (Driver otherDriver : drivers) {
                    if (otherDriver != currentDriver) {
                        if (shareGossip(allGossipSet, stopIndex, currentDriver, otherDriver, drivers)) {
                            return stopIndex + 1;
                        }
                    }
                }
            }
        }

        return 0;
    }

    private static boolean shareGossip(Set<String> allGossipSet, int stopIndex, Driver currentDriver, Driver otherDriver, Driver[] drivers) {
        boolean allGossipsShared = false;

        if (getStop(currentDriver, stopIndex) == getStop(otherDriver, stopIndex)) {
            currentDriver.getGossipSet().addAll(otherDriver.getGossipSet());
            otherDriver.getGossipSet().addAll(currentDriver.getGossipSet());

            if (allGossipsShared(allGossipSet, drivers)) {
                allGossipsShared = true;
            }
        }
        return allGossipsShared;
    }

    private static Set<String> getAllGossipSet(Driver[] drivers) {
        Set<String> gossipSet = new HashSet<>();

        for (Driver driver : drivers) {
            gossipSet.addAll(driver.getGossipSet());
        }

        return gossipSet;
    }

    private static int getStop(Driver driver, int index) {
        return driver.getRoute()[index % driver.getRoute().length];
    }

    private static boolean allGossipsShared(Set<String> allGossips, Driver[] drivers) {
        boolean allGossipsShared = false;

        for (Driver driver : drivers) {
            allGossipsShared = driver.getGossipSet().containsAll(allGossips);

            if (!allGossipsShared) {
                break;
            }
        }

        return allGossipsShared;
    }
}
